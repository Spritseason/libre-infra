# Description d'une infrastructure

le **D**ossier d'**A**rchitecture **T**echnique ou DAT est le document de référence d'une solution d'infrastructure.

Voici une proposition de plan de DAT.

## Objectif de la solution

On décrit brièvement les enjeux de la mise en oeuvre de cette solution et les métriques importantes récupèrées dans l'expression de besoin.

## Inventaire technologique

On recence ici les technologies matérielles et logicielles misent en oeuvre en y associant les éventuels contrats de support et les principes de gestion des anomalies associées.

Cette partie est rédigé par l'architecte de la solution et représente les périmettre de compétence associé à la gestion de cet infrastructure.

## Implantation physique du matériel

On retrouve ici les informations nécessaires à l'équipe datacenter pour la mise en place de la solution. Dans le cadre du déploiement de l'infrastructure matérielle.

* L'inventaire matériel détaillé
* Les positionements dans les racks et les consommations électriques associées.
* Les arrivées réseaux attendues

Cette section sera nécessaire aux achats puis au déploiement de la solution. elle peut être ammendé à postériori, par le prestataire (interne ou externe) responsable de l'intégration matérielle.

## Architecture Réseaux

### Présentation

Il convient de présenter les différents Vlan, subnet et éventuelement les VRF à mettre en oeuvre avec une description de ceux-ci précisant leurs usages. Un shéma de principe réseaux (par zone réseaux) est attendu ici

### Implantation logique

On précise alors le schéma précédent en ajoutant les instances réelles associées aux vlans et subnet (et VRF). cette partie est séparée car rédigée en regard de l'existant et de l'environnement déployé (différencier les instances preproduction et production)

### Implantation physique du réseaux

A destination de l'équipe datacenter et rédigée par l'intégrateur matériel ou l'équipe réseaux on précise ici les connexions réseaux attendues pour tous les équipements réseaux.

## Architecture système

### Le stockage

Si existant, les éléments autonomes de stockage (nas san) devront être présentés avec les principes d'usages :

* Classe de stockage (besoin en performances)
* Gestion des capacités et de la croissance
* Méthodes de sauvegardes

### Les roles

Décrire pour chaque host son rôle et pour chaque role les éléments communs à tout les hosts de ce rôle parmis les données suivantes :

* ressources : modèle, cpu, ram, disques et carte controleurs, carte réseaux...
* Données logiques :
  * Système d'exploitation
  * Gestion du stockage et modèle de partitionning et filesystèmes
  * Description des Connexions réseaux attendues par cartes réseaux
  * package logiciels `socle` à installer

### les fiches hosts

host par host on précise alors les valeurs attendues pour les instanciations du rôle : id-inventaire, nom, ip, wwn etc...

Ces informations nécessaires à la mise en oeuvre seront alors saisies dans les outils d'inventaire et d'exploitation.

## Architecture logicielle

Les équipes d'intégration logiciel précisent ici par host ou par rôle les logiciels et configurations attendus.

* Les services réseaux nécessaire mais externe à la solution (DNS, smtp etc...)
* Les logiciels `socle` et `applicatifs`
* Les flux réseaux applicatifs (la matrice des flux)

## Architecture fonctionnelle

Enfin on précise de façon simplifiée le modèle fonctionnel de la solution et les flux métiers associés à l'infrastruture.

Ce dernier point est lié à l'ingénieurie logicielle mais constitue une source d'information indispensable aux équipes d'exploitation et de support de la solution.

## Eléments suplémentaires

### notion d'environnements

Sur la présentation de la solution viendra s'ajouter la notion d'environnement : les instanciations / implantations sont effectuées **pour chaque environnement** :

* environnement de developpement, lab et/ou test
* environnement de staging, recette et/ou de pre-production
* environnement de production.

Chacuns des chapitres pourra alors être subdivisé par environnement.

### Gestion du MCO

Ou **M**aintien en **C**onditions **O**pérationnelles
Il conviendra d'ajouter les informations nécessaires à l'exploitation en général dans un second document :

* comment démarrer, arreter, sauvegarder, restaurer, monitorer la solution
* les procédures opérationnelles courantes : Les changements standards (Création/suppression d'une nouvelle instance de tel ou tel objet fonctionnel Exemple : ajout/suppression d'utilisateurs, ajout d'un LUN sur une baie)
* Le plan de production et l'odonnancement des traitements.

Ces éléments, **vivants** seront logés de façon très confortable dans un wiki ou dans dépot en markdown.

## En conclusion

Un besoin métier est défini. Afin d'y répondre, on précise les points cités ci dessus du dernier point (architecture fonctionnelle) au premier (implantation physique de l'inventaire matériel) au cours du projet de conception. En revanche le projet de déploiement lui suit le dossier dans le sens inverse du priemier point au dernier (cela rejoint le cycle en V).
