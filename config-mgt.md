# Gestion des configurations

## Contexte

Les infrastructures informatiques explosent (nombre de hosts / vm / services) et avec elles leur `entropie` respective.
Dans ce contexte de forte valence, il convient d'appliquer les bonnes pratiques permettant de maitriser à la fois son infrastructure, sa dette technique mais aussi les coûts de gestion de celle-ci. Les solutions opensource, de part leur ouverture, apporte un niveau d'autonomie vis a vis de ses fournisseurs.

## Objectifs

- La réduction des actions manuelles et des erreurs humaines
- La fiabilisation des déploiements au travers de leur automatisation
- Un meilleur suivi de la version globale du système d'information

## Principes

- La définition d'un état à atteindre plutot qu'une méthode pour l'atteindre.
- L'idempotence des actions de configuration comme vecteur de qualité.
- La modularité pour le [DRY](https://fr.wikipedia.org/wiki/Ne_vous_r%C3%A9p%C3%A9tez_pas) et le DRY.

## Limites de la gestion des configutation

- L'idempotence est en grande partie intégrée dans les modules de gestion de configuration mais elle reste incomplète. Il faut donc l'intégrer comme contrainte de développement pour l'automatisation.
- Le Rollback n'est pas intégré (la révocation de configuration)
  - En fait il n'est pas possible de gèrer cela au niveau de la gestion de conf car il faudrait connaitre précisément l'état initial. (Exemple de point bloquant : continuité de la production sur les données métier, difficulté de retour arrière sur une migration de schéma de données )
- les outils n'offrent pas non plus la gestion de la conformité (valider que l'état courrant est toujours à l'état attendu)
  - Cependant nous pouvons l'outiller en partie via l'utlisation du mode "--dry-run,--check,--dontdo" si le principe d'idempotence a été respectée.
- la gestion de conf et de ses limites apportent de nouvelles contraintes sur le processus de release managment (création de la release et déploiement).

## Versionning de l'infrastructure

On pourra versionner précisément l'ensemble d'une infrastrcuture de production de service lorsque l'ensemble de la configuration est maitrisé.

La version de l'infrastructure est conditionnée par :

- La version de l'implantation matérielle et physique
- La version des configurations d'infrastructure
- Les versions logiciels
- La version des configurations applicatives

Simple à suivre lors de la mise en place d'une nouvelle infrastructure, c'est en revanche plus complexe à remettre en place sur une infrastructure existante qui n'a pas continuellement gérer proprement ses configurations.
