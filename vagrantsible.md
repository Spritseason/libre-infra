# Intégration ansible dans Vagrant

## Presentation

Vagrant intègre complètement ansible il suffit pour cela de placer dans le dossier projet vagrant une arborescence Ansible minimal : 

- le playbook
- le dossier roles avec les roles

L'intéret est que : 

- Vagrant va pouvoir gérer l'inventaire pour nous (il a déja la liste des hosts)
- le compte utilisateur vagrant sera utilisé, pas besoin de créer un compte et de déposer la clef

Bref, c'est clef en main; exemple de provision ansible pour le playbook playbook.yml en utilisant le compte vagrant à la place du compte ansible:

```ruby
  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "playbook.yml"
    ansible.force_remote_user = true
  end
```

> regardez [plutôt](https://tse1.mm.bing.net/th?id=OIP.Wnf09pK2VfkHGyDAuopOMQHaHa&pid=Api&P=0&w=300&h=300) la documentation officiel pour voir toute les options possibles https://www.vagrantup.com/docs/provisioning/ansible.html

__Exercice:__

Vous corrigerez le `Vagrantfile` et le playbook du tuto Ansible afin d'utiliser l'intégration d'ansible dans vagrant, vous supprimerez aussi le superflux.
