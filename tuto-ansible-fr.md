# Ansible

Le Gestionaire de configuration "à la mode"

## Présentation

 * codé en python
 * effectue des tache sur des hosts distants
 * au travers d'une connexion ssh (avec utilisation de clef ssh)
 * avec un comportement idempotant

### vocabulaire

- Inventaire : Liste des hosts managé et rassemblé dans des groupes ; pour lesquels on définit des variables
- Playbook : Une "recette" de configuration comment on configure quoi sur quel hosts
- Roles : Une unité de configuration autonome et **réutilisable**
- Un module ansible : C'est un composant ansible "outil" de configuration, c'est un ensemble de taches possible de même type ou portant sur le même objet de configuration : utilisateur, fichier, etc ...
- Un node : Une cible identifiée pour être configuée.

## Utilisation

### Installation

Sur votre hosts d'aministration, à partir d'une invite de commande compatible bash ou équivalent. L'installation se fait avec pip après avoir installé python et python-pip:

```bash
sudo apt install python3 python3-pip
```

On pourra utiliser pip pour installer Ansible 
```bash
pip3 install --user ansible
```

Cependant il existe des packages pour la plupart des distributions

https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#basics-what-will-be-installed

**votre clef ssh devra être installé sur les hosts à gèrée, elle est le moyen d'accès aux hosts.**

### environnement de test

Dans un dossié dédié, vous créer le `Vagrantfile` suivant: 

```ruby
Vagrant.configure(2) do |config|
  (1..2).each do |i|
    config.vm.provision "ansible-user", type: "shell" do |cmd|
      ssh_pub_key = File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip
      cmd.inline = <<-SHELL
        useradd -m ansible -G wheel
        mkdir /home/ansible/.ssh
        chmod 700 /home/ansible/.ssh
        echo #{ssh_pub_key} >> /home/ansible/.ssh/authorized_keys
        chmod 600 /home/ansible/.ssh/authorized_keys
        chown -R ansible.ansible /home/ansible/.ssh
        sed -i 's/^# %wheel/%wheel/' /etc/sudoers
      SHELL
    end
    config.vm.define "target-#{i}" do |target|
      target.vm.box = "centos/7"
      target.vm.network "private_network", ip: "192.168.56.4#{i}"
      target.vm.hostname = "target-#{i}"
    end
  end
end
```

Avec la commande `vagrant up` : 
- vous créez deux VM de test 
- vous y créez un compte ansible 
- vous déposez votre clef publique ~/.ssh/id_rsa.pub pour pouvoir vous y connecter. (modifiez le vagrant file si votre clef a un autre nom)

> Les deux hosts sont alors accessibles sur leur adresse ip hosts-only : target-1 192.168.56.41 ; target-2 192.168.56.42

Connectez vous ansible sur les deux hosts une première fois pour intialiser la reconnaissance ssh entre avec ces hosts.

```bash
$ ssh ansible@192.168.56.41
The authenticity of host '192.168.56.41 (192.168.56.41)' can't be established.
ECDSA key fingerprint is SHA256:sYvb9BBLH8y+pVXr2ARwDCVzyvAWiIMwPqr9p1ZeSRs.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '192.168.56.41' (ECDSA) to the list of known hosts.
Last login: Tue Sep 10 20:10:19 2019 from 192.168.56.1
[ansible@target-1 ~]$ logout
Connection to 192.168.56.41 closed.
$ ssh ansible@192.168.56.42
The authenticity of host '192.168.56.42 (192.168.56.42)' can't be established.
ECDSA key fingerprint is SHA256:oT03vk/P0K8UjDJ3XGWKisVKvW6Z+UUXb8d5Kz3SniA.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '192.168.56.42' (ECDSA) to the list of known hosts.
Last login: Tue Sep 10 20:10:19 2019 from 192.168.56.1
[ansible@target-2 ~]$ logout
Connection to 192.168.56.42 closed.
$ 
```

### Inventaire

Ansible se base sur un inventaire de hosts appartenant à un groupe ou plusieurs group les actions sont ensuite effectuées sur un groupe de hosts ou un hosts.

__A faire:__

On se créer donc un dossier d'inventaire dans le dossier courant et on ajoute nos hosts target-1 et target-2 dans un groupe target

```bash
mkdir inventory
echo "[target]" > inventory/hosts
echo "192.168.56.41 hostname=target-1" >> inventory/hosts
echo "192.168.56.42 hostname=target-2" >> inventory/hosts
echo "" >> inventory/hosts
echo "[target:vars]" >> inventory/hosts
echo "variabledetarget=lavaleure" >> inventory/hosts
```

> vous noterez que nous avons ajouté la variable de host `hostname` aux deux nodes 192.168.56.41 et 192.168.56.42 et la variable de groupe `variabledetarget`.

> lorsque vous reviendrez sur ce sujet aller donc voir la doc officielle : https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html mais pas là.

Il est possible de créer dans le dossier `inventory` des dossiers hosts_vars et groups_vars contenant des fichiers yaml portant les noms des hosts ou des groupes et contenant les variables associées. Ce afin d'organiser un peu les données

### commandes ad-Hoc

**Avant de coder des playbook ansible** vous pouvez déjà utiliser la command `ansible` pour passer de simple commande distante sur vos hosts :

__A faire:__

Vous lancerez un ssh-agant puis vous y chargerez votre clef cela sera plus simple que de saisir sont mot de passe dce clef à chaque usage

```bash
ssh-agent bash
ssh-add ~/.ssh/id_rsa
```

On peu alors passe des commande sur nos hosts

```
$ ansible -i ./inventory/ target -a "echo hello" -u ansible
192.168.56.42 | CHANGED | rc=0 >>
hello

192.168.56.41 | CHANGED | rc=0 >>
hello
$
```

avec :

- `-i` pour préciser le dossier d'inventaire
- `-a` pour args , ici c'est juste une simpe commande à passer
- `-u` pour le user à utiliser sur le host (car ce n'est pas le votre)

En ajoutant l'option `--become` vous faites l'action sous root : 

```bash
$ ansible -i ./inventory/ target -a "id" -u ansible --become
192.168.56.41 | CHANGED | rc=0 >>
uid=0(root) gid=0(root) groups=0(root) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023

192.168.56.42 | CHANGED | rc=0 >>
uid=0(root) gid=0(root) groups=0(root) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
```

> **Attention**, avec votre compte et votre clef privée ssh locale, vous vous connectez **avec le compte ansible** sur le node distant ; enfin **ce dernier devient root** (avec sudo) pour passer la commande `id`

#### Les modules

Avec ansible vous allez utiliser des modules. Ceux-ci définisse les méthodes d'action sur une **classe d'objet système** géré par ansible.

Vous definissez l'état attendu sur l'objet géré avec le module 

__exemple :__

Livrer un fichier avec le module copy:

```bash
ansible -i ./inventory/ target -m copy -a "src=./inventory/hosts dest=/tmp/hosts" -u ansible
```

Gérer les inodes (les attributs de fichier) avec le module file

```bash
ansible -i ./inventory/ target -m file -a "dest=/tmp/hosts mode=600 owner=ansible state=file" -u ansible --become
```

Gérer les installations de packages rpm avec le module yum

```bash
ansible -i ./inventory/ target -m yum -a "name=epel-release state=present" -u ansible
```

gérer les services

```bash
ansible -i ./inventory/ target -m service -a "name=httpd state=restarted" -u ansible
```

> Les nombreux modules d'ansible sont documentés ici : https://docs.ansible.com/ansible/latest/modules/list_of_all_modules.html . Alors aller chercher l'information là ou elle est :).

## Ecrire du code ansible

L'objectif est de gérer la configuration d'une infrastrcuture. Avec Ansible on défini l'état attendu et donc les actions à réaliser sur les hosts pour atteindre cet état.

### le yaml

Le code Ansible c'est du yaml : **Y**et **A**nother **M**eta **L**anguage , c'est un langage descriptif, il peu contenir notamment :

- une liste : Une suite d'entrée **de même indentation** préfixé par un `-` 

  ```yaml
    - élément1
    - élément2
    - élément3
  ```

- Un dictionaire (dict) : un enchainement de `clef: valeur` . Le séparateur est la suite : **deux points suivis par un espace**. La valeur peu être une simple valeur, une liste ou un autre dictionnaire.

  ```yaml
  - player1:
      name: rick
      friends:
          - morty
          - bart
  - player2:
      name: morty
      friends:
          - bart
  ```

  Nous avons ici une liste de dictionaire player1, player2 dont les valeurs sont des dicts de deux paires de clef valeurs : name et friends et la clef friends à pour valeur une liste.

Vous noterez que l'indentation est **trèèès importante**.

> C'est en vérité un peu plus complexe et riche que ça mais cela est sufisant pour commencer. RTFM point! Vous regarderez la doc pour aller plus loin : https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html

### Les Playbook

Donc codé en Yaml, nous allons definir, pour une séries de hosts, une suite d'action à réaliser pour atteindre un état.

__A faire:__

En exemple, créez le fichier [apache-base-config.yml](./files/apache-base-config.yml) dans le dossier courant et copier ce contenu :

```yaml
- name: apache base config
  hosts: target
  user: ansible
  become: yes

  vars:
    http_port: 80
    serveur_admin: webmaster@lab.local

  tasks:
  - name: ensure apache is up to date
    yum:
      name: httpd
      state: latest
  - name: write the apache config file
    template:
      src: templates/httpd.j2
      dest: /etc/httpd/conf/httpd.conf
      backup: yes
    notify:
    - restart apache
  - name: ensure apache is running
    service:
      name: httpd
      state: started

  handlers:
    - name: restart apache
      service:
        name: httpd
        state: restarted
```

Je l'explique :

1. On commence par décrire le playbook et comment il va opèrer : 
    
   ```yaml
   - name: apache base config
     hosts: target
     user: ansible
     become: yes
   ```
  
   Quelle sont les cibles du playbook (le groupe target) avec quel utilisateur (ansible) on se connecte, est-ce qu'on deviens root (become: yes) ?
    
2. puis on précise des variables de playbook
   
   ```yaml
     vars:
       http_port: 80
       serveur_admin: webmaster@lab.local
   ```
   
3. Une liste de taches (task) à effectuer, c'est l'équivalent des commandes passée plus hautrs : un module avec des arguement
   
   ```yaml
     tasks:
     - name: ensure apache is up to date
       yum:
         name: httpd
         state: latest
     - name: write the apache config file
       template:
         src: templates/httpd.j2
         dest: /etc/httpd/conf/httpd.conf
         backup: yes
       notify:
       - restart apache
     - name: ensure apache is running
       service:
         name: httpd
         state: started
   ```
   
4. Un handler c'est tache qui est déclenché par une notification qu'une autre tache à due être exécuté. Les handler sont traité à la fin.
   
   ```yaml
     handlers:
       - name: restart apache
         service:
           name: httpd
           state: restarted
   ```

__A faire:__

Vous pouvez récupèrer le fichier **templat**e [httpd.j2](./files/httpd.j2) et le placer dans un dossier templates dans le dossier courant.

puis vous pourrez alors tester ce playbook :

```bash
ansible-playbook -i inventory apache-base-config.yml
```

#### Les taches

Chaque tache définit un état à ateindre sur un objet de configuration grace à un module, en résultat la tache est soit :

- OK (déja fait rien a faire),
- changed (la tache à été exécuté avec succes),
- failed (la tache n'a pas pu être exécuté) le reste du playbook est avorté

Leur définition peu inclure des conditions, Exemple en utilisant la clause `when:`
```yaml
- name: install apache
  yum:
    name: "httpd"
    state: present
  when: ansible_facts['os_family']|lower == 'redhat'
```

On reparlera des facts un peu plus loin, mais vous devriez avoir compris le principe.

> La clé 'when: ' est un argument de tache et non un argument du module d'ou l'indentation lié à la tache


#### Les rôles

L'objectif est de disposer de briques réutilisable. Le playbook est lui statique, il définit comment on applique une configuration sur un ensemble de host. 
En definissant un role, on créé une brique indépendante de configuration qui sera utilisable par plusieurs playbook.

Le role est définit dans une arborescance précise. Celle-ci se situe dans le dossier `roles` lui même situé au même niveau que le playbook Celle-ci permet d'organiser sa configuration.

```bash
$ tree roles
roles
└── apache-base-config
    ├── defaults
    │   └── main.yml
    ├── files
    ├── handlers
    │   └── main.yml
    ├── meta
    ├── tasks
    │   └── main.yml
    ├── templates
    │   └── httpd.j2
    └── vars
        └── main.yml
```

Dans ce cas le playbook sera baucoup plus simple :

```yaml
- name: apache base config
  hosts: target
  user: ansible
  become: yes
  roles:
    - apache-base-config
```

> notez bien ce playbook vous aller en avoir besoin un peu plus bas

__Les dossiers pour les fichiers yaml ansible:__

- tasks : les taches à valider ou exécuté
- handlers : les handlers dédié au role
- defaults : les variables par défaut du role
- vars : les variables du role
- meta : la définition des méta données lié au role

Pour ces dossier, le fichier principale est main.yml mais on peu inclure d'autres fichiers.

Exemple:

On peu inclure des taches spécifique à une distribution : 

```yaml
- name: added in 2.4, previously you used 'include'
  import_tasks: redhat.yml
  when: ansible_facts['os_family']|lower == 'redhat'
```

> Vous noterez l'usage du filtre `|lower`. Les filtres sont puissant et trés riche en possibilité : https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html

Pour inclure des variables on utliser une tache d'inclusion : 

```yaml
- name: Gather OS specific variables
  include_vars: "{{ item }}"
  with_first_found:
    - "{{ ansible_distribution|lower }}-{{ ansible_distribution_version }}.yml"
    - "{{ ansible_distribution|lower }}-{{ ansible_distribution_major_version }}.yml"
    - "{{ ansible_distribution|lower }}.yml"
    - "{{ ansible_os_family|lower }}.yml"
```

> with_first_found: vas chercher le premier fichier qui matche pour l'importer. L'objectif etant de rendre le role utilisable sur toutes les distributions on pourra préciser des variable différente dans des fichiers judicieusement nommé (cf: Facts).

__Les dossiers pour les fichier annexes:__

- files contiens les fichiers utilisé par le role
- templates contiens des templates de fichiers (on reviendra sur le templating plus loin)

> encore une fois (RTFM point) la doc officielle est a consulter : https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html mais plutot une prochaine fois.

__Exercice :__

Vous créez cette arborescence de fichier et vous distriburez dans les fichiers main.yml les parties qui n'aparaissent plus dans le playbook sité plus haut.

#### Les variables

Les taches, les handler et leurs conditions ainsi que toute la config vien dépendre de variables qui sont définie et sont utilisé à plusieurs endroits.

__les facts:__

Chaques hosts retourne à ansible un ensemble de variables qui le concernes, le hostname, le nom de la distribution, ses adresse ip etc...

ces variable sont utilisable via le dictionaire ansible_facts:

```bash
{{ ansible_facts['nodename'] }}
```

> pour comprendre, regarder tout ce qui est collecté par ansible : https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variables-discovered-from-systems-facts


__precedences des variables:__

Des règles de précédence viennents régire l'odres avec lequel les variables peuvente être surchargées. En exemple, les variables par défaut du role peuvent être surchargées lorsqu'elle sont redéfinie en variables de groupes.


1. les valeurs saisies en ligne de commande (-u user)
2. les variables par défaut définient dans les roles (defaults/main.yml)
3. les variables de groupe de l'inventaire (group_vars/groupe.yml)
4. les variables de hosts définies dans l'inventaire (hosts_vars)
5. les facts (voir ci dessous)
6. les variables de playbook (vars:)
7. les variables de role (vars/main.yml)
8. les extra vars (surchargent toutes les autres)

![](./images/ansible-var-usage.png)

__Exercice:__

Vous ajouterez une structure de variable de Groupe décrivant les composant d'un name virtual host ( documentroot, servername, socket d'écoute )

#### Templating avec jinja2

On peu utiliser Jinja2 pour templater des fichiers de config à livrer sur des nodes :

```bash
$ cat templates/httpd.j2 
ServerRoot "/etc/httpd"
Listen "{{ ansible_facts['eth1']['ipv4']['address'] }}:{{ http_port }}"
Include conf.modules.d/*.conf
User apache
Group apache
ServerAdmin "{{ serveur_admin }}"
<Directory />
.../...
```

Ici on utilise des variables de Ansible (la concatènation de chaine de caratère est implicite)

On utilise aussi le templating dans le code Ansible mais ce n'est pas du pure jinja2

On vas notamment pouvoir faire des boucle afin d'éviter de répeter du code yaml : https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html 

Et pas mal d'autre choses qui viendront en temps voulu, je crois qu'on est bon pour créer un fichier virtualhost  pour la structure de donnée que vous avez créer un peu plus haut.

__Exercice:__

Vous crérez donc les taches permettant de créer des virtualhost. Vous devez alors trouver comment faire une boucle sur une liste de dict.

## Ansible galaxy

La cerise sur le gateau c'est que beaucoup de rôles réutilisable déjà développés sont disponible sur ansible-galaxy, il est donc possible d'en télécharger et c'est du clef en main.

Ansible galaxy télécharge et dépose (sauf configuration spécirfique) les roles : 

Dans `$HOME/.ansible/roles` au niveau utilisateur
Dans `/usr/share/ansible/roles` au niveau partage ansible sur le système
Dans `/etc/ansible/roles` pour un serveur de configuration ansible en ligne de commande

La commande `ansible-playbook` recherchera les roles dans ces dossiers en plus du dossier role situé à coté du playbook.

> https://docs.ansible.com/ansible/latest/galaxy/user_guide.html

### Usage

```bash
$ ansible-galaxy --help
Usage: ansible-galaxy [delete|import|info|init|install|list|login|remove|search|setup] [--help] [options] ...

Perform various Role related operations.

Options:
  -h, --help            show this help message and exit
  -c, --ignore-certs    Ignore SSL certificate validation errors.
  -s API_SERVER, --server=API_SERVER
                        The API server destination
  -v, --verbose         verbose mode (-vvv for more, -vvvv to enable
                        connection debugging)
  --version             show program's version number, config file location,
                        configured module search path, module location,
                        executable location and exit
```

Recherche de role :

```bash
$ ansible-galaxy search --galaxy-tags powerdns

Found 10 roles matching your search:

 Name                            Description
 ----                            -----------
 alainvanhoof.alpine_powerdns    PowerDNS for Alpine Linux
 hspaans.pdns-ansible            Install and configure the PowerDNS Authoritative DNS Server
 opsta.pdns_admin                Ansible role to install PowerDNS Admin
 pari-.pdns-recursor             An Ansible role which installs and configures PowerDNS recursor
 PowerDNS.pdns                   Install and configure the PowerDNS Authoritative DNS Server
 PowerDNS.pdns_recursor          Install and configure the PowerDNS Recursor
 sparknsh.pdns                   Install and configure the PowerDNS Authoritative DNS Server
 stuvusIT.dnsdist                Install and configure a dnsdist server
 stuvusIT.pdns-authoritative     Install and configure an authoritative PowerDNS server
 stuvusIT.pdns-authoritative-api Configure PowerDNS zones via the HTTP API
```

Un exemple de role assez complet à étudier :
https://github.com/lae/ansible-role-netbox 

__Exercice:__

liser le readme, et installez cette application : netbox
avec un vagrantfile qui déploi une VM centos et un provisionning ansible qui déplois netbox. Vous devrez installer deux autre role avec ansible-galaxy.
