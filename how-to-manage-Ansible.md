# Ressources et Discussion

## Les bonnes pratiques Ansible

[RTFM : ansible best practices](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html) 

Discussion :

- comment gèrer proprement les environements : Lab / preprod / prod ?

## Tester ses playbook

[RTFM : Ansible test strategies](https://docs.ansible.com/ansible/latest/reference_appendices/test_strategies.html)

Discussion :

- Comment plliquer les même playbooks sur tout les environements : lab, stagging, prod ?
- Comment pouvons nous automatiser alors la gestion des opération sur l'infra ?

## gestion de la configuration de l'infra

- Un seul gros dépot Ansible ?
- Des dépots par plateforme et par rôle puis utilisation de ansible-galaxy requirement.yml et du site.yml ?
- Une autre solution ?
