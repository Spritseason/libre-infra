# Plan de cours

## Présentation de la majeure

[descriptif](./descriptif.md) du cours

- Open source vs [logiciel libre](./logiciel-libre.md)
- Principes de [gestion d'une infrastructure](./gestion-infra-run.md)
- La [maturité des systèmes d'informations](./maturite-SI.md)

### Projet

- [Présentation du projet](./projet.md)
- Description d'une infrastructure : [le DAT](./description-infra.md)

### Environnement de travail

Nous utiliserons en local : Virtualbox, Docker, Vagrant, Ansible et Molecule.

Un système GNU/linux accèdant aux fonctionalités de virtualisation du CPU est me semble-t-il la solution la plus simple à mettre en oeuvre (en natif ou en dual boot) vous aurez besoin de disposer de **rsync, ssh-client, vagrant, docker-ce, docker-compose, python, python-dev. Puis avec pip: ansible, molecule, molecule[docker], python-vagrant, molecule[vagrant]**

Pour les utilisateurs windows (version 10 Build 17063 minimum) ne disposant pas de dual boot il conviendra de :

- Avoir VirtualBox sous windows (version 5.2 préférée)
- Avoir docker sous windows
- Avoir WSL (version 1 sous ubuntu 18.04 préféré)
- Bien configurer WSL afin de disposer de :
  - la gestion des droits posix
    /etc/wsl.conf :
    
    ```bash
    [automount]
    enabled = true
    options = "metadata"
    ```

  - disposer de **rsync, ssh-client, python-pip, ansible, vagrant, docker-ce, docker-compose, molecule, molecule[docker], python-vagrant**
  - disposer du chemin vers les commandes virtualbox de windows
    
    ```bash
    export PATH="$PATH:/mnt/c/Program Files/Oracle/VirtualBox"
    ```

  - Activer l'utilisation du Virtualbox installé sous windows et définir le home du user pour vagrant sur le FS windows (sous drvfs)

    ```bash
    export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"
    export VAGRANT_WSL_WINDOWS_ACCESS_USER_HOME_PATH="/mnt/c/users/votre-login"
    ```

  - préciser à la cli docker et docker compose comment se connecter au daemon docker :

    ```bash
    export DOCKER_HOST=tcp://localhost:2375
    ```

  - dans certains cas il peu être nécessaire de préciser les chemin vers le powershell et les commande systeme32
    ```bash
    export PATH="$PATH:/mnt/c/Windows/System32/WindowsPowerShell/v1.0/"
    export PATH="$PATH:/mnt/c/Windows/System32/" #cmd.exe
    ```

## L'automatisation

### La gestion de conf avec Ansible

- Présentation de [la gestion des configuration](./config-mgt.md)
- Présentation de l'outil [Vagrant](./vagrant.md) suivi de petits [exercices](./TD-vagrant.md) de prise en main.
- [Tutoriel Ansible](./tuto-ansible-fr.md)
- [Intégration Ansible dans Vagrant](./vagrantsible.md)
- [Discussion](./how-to-manage-Ansible.md) autour de la gestion des configuration avec ansible

### Ecriture et teste de roles Ansible avec Molecule

- Présentation de [Molecule](molecule.md)

## Gestion de la disponibilité

- Les principes de la gestion de la [disponibilité](./disponibilite.md)
- [Configuration réseaux](./config-net-advanced.md)  avancée sous linux
- Le couteau suisee réseaux [keepalived](./keepalived.md)
- La [réplication Mysql](./mysql-replication.md)
