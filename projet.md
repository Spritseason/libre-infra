# Présentation projet

## Cadre

Afin de valider la majeure. Vous devrez mettre en place à 3 ou 4 personnes une infrastructure offrant un ou plusieurs services basés sur des solutions opensources tout en applicant les bonnes pratiques de gestion des infrastructures.

Vous devrez mettre en oeuvre les principes :

- d'automatisation de la gestion de votre infrastucture,
- de gestion des configurations,
- de gestion de la disponibilté.

Le service founi doit être bien défini, c'est la finalité de votre projet d'infratructure.

## Rendu

### Oral

Le jeudi 19/12, tous les groupes présentent leur projet à l'ensemble des autres groupes.

La présentation Orale devra mettre en valeur le travail effectué et ce qu'il apporte.

### Ecrit

Le rendu du projet sera accompagné d'un dossier présentant :

* le contexte
* la documentation de l'architecture
* la documentation du MCO (maintien en conditions opérationnelles), au minimum :
  * Le redémarrage et gestion des log des services
  * Les mises à jour
  * la documentation sur les indicateurs de monitoring
    * qu'est-ce qui doit être surveillé et comment?
  * la documentation de sauvegarde et des purges
    * qu'est-ce qui doit être sauvegardé et purgé et comment?
