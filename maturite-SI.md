# Maturité des systèmes d'information

## Modèle de mesure de la maturité

Le modèle suivant est utilisé pour evaluer la maturité de plusieurs pratiques informatiques tel la gestion de projet ou la gestion de la sécurité des systèmes d'information ou simplement la maturité de l'entreprise dans son ensemble.

Il présente 5 niveaux de maturité

1. Niveau initiale : On se contente de faire de manière informelle.
2. Gérée : Les opérations sont planifiées, suivies et tracées
3. Processé : documentation des processus de réalisation, publiés et bien compris.
4. Evalué : Surveillance des processus avec mesures quantitatives.
5. Amélioration continue : On tient compte de l'évolution du contexte et les processus sont mis à jour régulièrement.

Cela reste très théorique, mais dans le concret on peut résumer ça de la façon suivante

1. Les actions sont simplement faites par les personnes qui savent faire.
2. D'autres personnes peuvent suivre les actions et peuvent alors planifier en conséquence (outils de ticketing ou de traçage)
3. Les méthodes de réalisation sont documentées (ceux qui savent ont décrit leur méthode, il est plus simple de les remplacer si besoin)
4. On mesure quantitativement (On classe et compte les opérations)
5. Une charge de travail est dédiée à l'amélioration des processus et méthodes (la qualité est prise en charge)

Ressources :

- CMMI : [Capability Maturity Model Integration](https://fr.wikipedia.org/wiki/Capability_Maturity_Model_Integration)
- Guide de l'ANSI sur la maturité SSI : https://www.ssi.gouv.fr/guide/guide-relatif-a-la-maturite-ssi/
