# Molecule

## Presentation

Molecule est un outils simplifiant le dévelopement de rôle ansible.

Cet outil implante dans la structure arborescente d'un rôle ansible une sous arborescance molecule dédiée à la validation et au test de celui-ci.

> https://molecule.readthedocs.io/en/stable/

## Definitions

- role : le role ansible
- driver : c'est le provider d'instance de test (docker, vagrant, LXC, LXD, Openstack, Linode, azure, digital ocean, EC2 etc...)
- instance : un node au sens Ansible, un "host"
- platform : un ensemble cohérent d'instance
- scenario : un scenario est un plan de test complet définissant une plateforme avec un driver et une séquence d'opérations

## quick start

Aller hop, on teste ça.

> largement inspiré de : https://molecule.readthedocs.io/en/stable/getting-started.html

On créer un nouveau rôle :

```bash
$ molecule init role -r my-new-role
--> Initializing new role my-new-role...
Initialized role in /home/alan/my-new-role successfully.
$ tree /home/alan/my-new-role/
/home/alan/my-new-role/
├── defaults
│   └── main.yml
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── molecule
│   └── default
│       ├── Dockerfile.j2
│       ├── INSTALL.rst
│       ├── molecule.yml
│       ├── playbook.yml
│       └── tests
│           ├── __pycache__
│           │   └── test_default.cpython-35.pyc
│           └── test_default.py
├── README.md
├── tasks
│   └── main.yml
└── vars
    └── main.yml

9 directories, 12 files
```

Molecule à créer un role avec les main.yml et les dossiers defaults, handlers, meta, tasks et vars ansi qu'une arborescance molecule.
Dans celle-ci, chaque dossier est un scenario de test, le fichier `molecule.yml` défini ce scenario, et le playbook.yml le playbook (utilisant ce role) qui sera testé.

```bash
$ cd /home/alan/my-new-role
```

On ajoute simplement une tache debug au nouveau role :

```bash
$ vi tasks/main.yml 
$ cat tasks/main.yml
---
# tasks file for my-new-role
- name: Hi there
  debug:
    msg: Hi, there! seems it's works
```

puis on lance les tests :

```bash
$ molecule test
.../...
```

Nous avons des erreurs pour le linter 'Ansible lint'
```bash
    [701] Role info should contain platforms
    [703] Should change default metadata: author
    [703] Should change default metadata: description
    [703] Should change default metadata: company
    [703] Should change default metadata: license
```

Nous corrigons simplement en éditant le fichier meta/main.yml :
```yaml
$ grep -v "^ *#" meta/main.yml | grep .
---
galaxy_info:
  author: Alan SIMON
  description: hi there role
  company: i'am with the devil
  license: GPLv3
  min_ansible_version: 1.2
  platforms:
  - name: centos
    versions:
    - all
  - name: debian
    versions:
    - all
  galaxy_tags: []
dependencies: []
```


Si on reteste :
```bash
$ molecule test
--> Validating schema /home/alan/my-new-role/molecule/default/molecule.yml.
Validation completed successfully.
--> Test matrix
    
└── default
    ├── lint
    ├── dependency
    ├── cleanup
    ├── destroy
    ├── syntax
    ├── create
    ├── prepare
    ├── converge
    ├── idempotence
    ├── side_effect
    ├── verify
    ├── cleanup
    └── destroy
```
Le scenario default et sa séquence d'opérations

- lint : validation syntaxique et de la forme du code de l'ensemble de la configuration molecule et du role.
- dependency : gestion des dépendance (avec galaxy ou glint)
- cleanup : exécution d'un playbook de "netoyage" notament utiliser pour netoyer des ressources externe à la plateforme de test
- destroy : destruction de l plateforme de test
- create : création de la plateforme
- prepare : run d'un playbook gérant des pré-requis attendu par le role (gestion de l'inventaire et des facts par exemple)
- converge : run du playbook de déploiement
- idempotence : second run avec vérification qu'aucune task n'est à l'état "changed"
- side_effect : run d'un playbook générant des évènement extérieur à la plateforme (exemple test de failover, feature expérimental)
- verify : run de tests sur la plateforme après le déploiement. Il conviens de tester ici le fonctionnel, pas la bonne exécution du playbook. plusieur module de test existent.

Les linter yaml, et ansible Lint pour le role puis Flake8 pour les tests :
```bash
--> Scenario: 'default'
--> Action: 'lint'
--> Executing Yamllint on files found in /home/alan/my-new-role/...
Lint completed successfully.
--> Executing Flake8 on files found in /home/alan/my-new-role/molecule/default/tests/...
Lint completed successfully.
--> Executing Ansible Lint on /home/alan/my-new-role/molecule/default/playbook.yml...
Lint completed successfully.
```

PRéparation de l'environnement : dependence, netoyage et destruction éventuel d'un eplateforme déja existante : 

```bash
--> Scenario: 'default'
--> Action: 'dependency'
Skipping, missing the requirements file.
--> Scenario: 'default'
--> Action: 'cleanup'
Skipping, cleanup playbook not configured.
--> Scenario: 'default'
--> Action: 'destroy'
--> Sanity checks: 'docker'
    
    PLAY [Destroy] *****************************************************************
    
    TASK [Destroy molecule instance(s)] ********************************************
    changed: [localhost] => (item=instance)
    
    TASK [Wait for instance(s) deletion to complete] *******************************
    ok: [localhost] => (item=None)
    ok: [localhost]
    
    TASK [Delete docker network(s)] ************************************************
    
    PLAY RECAP *********************************************************************
    localhost                  : ok=2    changed=1    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0
```

Création de l'environnement avec Ansible !

```bash
--> Scenario: 'default'
--> Action: 'syntax'
--> Sanity checks: 'docker'
    
    playbook: /home/alan/my-new-role/molecule/default/playbook.yml
--> Scenario: 'default'
--> Action: 'create'
    
    PLAY [Create] ******************************************************************
    
    TASK [Log into a Docker registry] **********************************************
    skipping: [localhost] => (item=None) 
    
    TASK [Create Dockerfiles from image names] *************************************
    changed: [localhost] => (item=None)
    changed: [localhost]
    
    TASK [Determine which docker image info module to use] *************************
    ok: [localhost]
    
    TASK [Discover local Docker images] ********************************************
    ok: [localhost] => (item=None)
    ok: [localhost]
    
    TASK [Build an Ansible compatible image (new)] *********************************
    ok: [localhost] => (item=molecule_local/centos:7)
    
    TASK [Build an Ansible compatible image (old)] *********************************
    skipping: [localhost] => (item=molecule_local/centos:7) 
    
    TASK [Create docker network(s)] ************************************************
    
    TASK [Determine the CMD directives] ********************************************
    ok: [localhost] => (item=None)
    ok: [localhost]
    
    TASK [Create molecule instance(s)] *********************************************
    changed: [localhost] => (item=instance)
    
    TASK [Wait for instance(s) creation to complete] *******************************
    FAILED - RETRYING: Wait for instance(s) creation to complete (300 retries left).
    changed: [localhost] => (item=None)
    changed: [localhost]
    
    PLAY RECAP *********************************************************************
    localhost                  : ok=7    changed=3    unreachable=0    failed=0    skipped=3    rescued=0    ignored=0
    
```

Puis préparation et exécution du playbook :

```bash
--> Scenario: 'default'
--> Action: 'prepare'
Skipping, prepare playbook not configured.

--> Scenario: 'default'
--> Action: 'converge'
    
    PLAY [Converge] ****************************************************************
    
    TASK [Gathering Facts] *********************************************************
    ok: [instance]
    
    TASK [my-new-role : Hi there] **************************************************
    ok: [instance] => {
        "msg": "Hi, there! seems it's works"
    }
    
    PLAY RECAP *********************************************************************
    instance                   : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    
```

et enfin validation de l'idempotence, les sides effects et le test du résultat :
```bash
--> Scenario: 'default'
--> Action: 'idempotence'
Idempotence completed successfully.
--> Scenario: 'default'
--> Action: 'side_effect'
Skipping, side effect playbook not configured.
--> Scenario: 'default'
--> Action: 'verify'
--> Executing Testinfra tests found in /home/alan/my-new-role/molecule/default/tests/...
    ============================= test session starts ==============================
    platform linux -- Python 3.5.2, pytest-5.2.2, py-1.8.0, pluggy-0.13.0
    rootdir: /home/alan/my-new-role/molecule/default
    plugins: testinfra-3.2.0
collected 1 item                                                               
    
    tests/test_default.py .                                                  [100%]
    
    ============================== 1 passed in 3.30s ===============================
Verifier completed successfully.
--> Scenario: 'default'
--> Action: 'cleanup'
Skipping, cleanup playbook not configured.
--> Scenario: 'default'
--> Action: 'destroy'
    
    PLAY [Destroy] *****************************************************************
    
    TASK [Destroy molecule instance(s)] ********************************************
    changed: [localhost] => (item=instance)
    
    TASK [Wait for instance(s) deletion to complete] *******************************
    FAILED - RETRYING: Wait for instance(s) deletion to complete (300 retries left).
    changed: [localhost] => (item=None)
    changed: [localhost]
    
    TASK [Delete docker network(s)] ************************************************
    
    PLAY RECAP *********************************************************************
    localhost                  : ok=2    changed=2    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0
    
--> Pruning extra files from scenario ephemeral directory
```

## Configuration

le dossier molécule contiendra les playbooks : playbook.yml(converge), prepare.yml, clean.yml, et side_effect.yml ; le fichier requirement.txt ; et les tests implémentés.

le fichier molecule.yml vien orchestrer tout ça :

```yaml
---
dependency:
  name: galaxy
driver:
  name: docker
lint:
  name: yamllint
platforms:
  - name: instance
    image: centos:7
provisioner:
  name: ansible
  lint:
    name: ansible-lint
verifier:
  name: testinfra
  lint:
    name: flake8
```


On notera ici :

- la gestion des dépendance avec galaxy (requirement.yml)
- le driver d'instances : docker
- le Linter globale **ainsi que sur le provisionner et le verifier.**
- la description de plateforme : une instance centos-7
- le provisioner ansible
- et enfin les tests `testinfra` le driver de test python (voir molecule/default/tests/test_default.py) et la doc : https://testinfra.readthedocs.io/en/latest/


> RTFM : https://molecule.readthedocs.io/en/stable/configuration.html

## Usage

Nous pouvons utiliser les scénarios complet molecule pour dérouler des test complets mais nous pouvons aussi simplement lancer une des partie du scénrario :

```bash
$ molecule create
--> Validating schema /home/alan/my-new-role/molecule/default/molecule.yml.
Validation completed successfully.
--> Test matrix
    
└── default
    ├── dependency
    ├── create
    └── prepare
    
.../...
```

et ainsi disposer d'un environement pour le dev consultable en interactif :

```bash
$ molecule login
--> Validating schema /home/alan/my-new-role/molecule/default/molecule.yml.
Validation completed successfully.
[root@instance /]# exit
$ 
```

Jouer son role dessus (converge)
```bash
$ molecule converge
--> Validating schema /home/alan/my-new-role/molecule/default/molecule.yml.
Validation completed successfully.
--> Test matrix
    
└── default
    ├── dependency
    ├── create
    ├── prepare
    └── converge
    
--> Scenario: 'default'
--> Action: 'dependency'
Skipping, missing the requirements file.
--> Scenario: 'default'
--> Action: 'create'
Skipping, instances already created.
--> Scenario: 'default'
--> Action: 'prepare'
Skipping, prepare playbook not configured.
--> Scenario: 'default'
--> Action: 'converge'
    
    PLAY [Converge] ****************************************************************
    
    TASK [Gathering Facts] *********************************************************
    ok: [instance]
    
    TASK [my-new-role : Hi there] **************************************************
    ok: [instance] => {
        "msg": "Hi, there! seems it's works"
    }
    
    PLAY RECAP *********************************************************************
    instance                   : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    
```

Repeter les deux action précente pendant votre dèv

en enfin valider l'idempotence :
```bash
$ molecule idempotence
--> Validating schema /home/alan/my-new-role/molecule/default/molecule.yml.
Validation completed successfully.
--> Test matrix
    
└── default
    └── idempotence
    
--> Scenario: 'default'
--> Action: 'idempotence'
Idempotence completed successfully.
```

et détruire l'environement
```bash
$ molecule destroy
--> Validating schema /home/alan/my-new-role/molecule/default/molecule.yml.
Validation completed successfully.
--> Test matrix
    
└── default
    ├── dependency
    ├── cleanup
    └── destroy
    
--> Scenario: 'default'
--> Action: 'dependency'
Skipping, missing the requirements file.
--> Scenario: 'default'
--> Action: 'cleanup'
Skipping, cleanup playbook not configured.
--> Scenario: 'default'
--> Action: 'destroy'
    
    PLAY [Destroy] *****************************************************************
    
    TASK [Destroy molecule instance(s)] ********************************************
    changed: [localhost] => (item=instance)
    
    TASK [Wait for instance(s) deletion to complete] *******************************
    FAILED - RETRYING: Wait for instance(s) deletion to complete (300 retries left).
    changed: [localhost] => (item=None)
    changed: [localhost]
    
    TASK [Delete docker network(s)] ************************************************
    
    PLAY RECAP *********************************************************************
    localhost                  : ok=2    changed=2    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0
    
--> Pruning extra files from scenario ephemeral directory
```

## Tuneable

En configurant le fichier molecule.yml on pourra définir précisément son environement de test (les instances, les images docker) et ce que l'on souhaite tester :
Il sera alors possible :

- de créer/modifier les playbooks playbook.yml(converge), prepare.yml, clean.yml, et side_effect.yml ;  
- gèrer les dépendances galaxy avec un fichier requirement.txt ; 
- et implémenter ses tests (dossier tests/)

Il est part ailleur aussi possible de créer d'autre scenarios de test (init scenario -r role -s nom-scenrario -d driver), basé sur un driver vagrant par exemple:

```bash
$ molecule init scenario -r my-new-role -d vagrant -s vagrant
--> Initializing new scenario vagrant...
Initialized scenario in /home/alan/my-new-role/molecule/vagrant successfully.
$ tree molecule/vagrant/
molecule/vagrant/
├── INSTALL.rst
├── molecule.yml
├── playbook.yml
├── prepare.yml
└── tests
    ├── __pycache__
    │   └── test_default.cpython-35.pyc
    └── test_default.py
```

Modifié pour utiliser une box centos/7 car elle existe déja en locale.

```yaml
molecule/vagrant/molecule.yml:
---
dependency:
  name: galaxy
driver:
  name: vagrant
  provider:
    name: virtualbox
lint:
  name: yamllint
platforms:
  - name: instance
    box: centos/7
provisioner:
  name: ansible
  lint:
    name: ansible-lint
verifier:
  name: testinfra
  lint:
    name: flake8
```

```bash
$ molecule create -s vagrant
--> Validating schema /home/alan/my-new-role/molecule/vagrant/molecule.yml.
Validation completed successfully.
--> Validating schema /home/alan/my-new-role/molecule/default/molecule.yml.
Validation completed successfully.
--> Test matrix
    
└── vagrant
    ├── dependency
    ├── create
    └── prepare

```

## conslusion

Voilà de quoi s'amuser a développer des role ansible réutilisable, testé et appliquant les bonnes pratiques.
