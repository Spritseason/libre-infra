# Exercice Vagrant

## Premiers pas

Vous trouverez dans le fichier [TP-Vagrant-files.tgz](./TP-Vagrant-files.tgz), un environement déja prêt pour ces exercices, il vous faudra extraire cette archive sur votre environnment de travail puis il sufira de renommer les fichiers Vagrantfile-1/2/3 en Vagrantfile pour chaqun des premiers TD

### Juste une VM

Demarrez une simple VM CentOS/7 :

```bash
~ $ mkdir -p work/first-test  # ou cd work/first-test, Ajustez suivant votre cas
~ $ cd work/first-test
first-test $ vagrant init centos/7
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```

Avec la commande `vagrant init` vous créez un Vagrantfile basic avec un grand nombre d'options commentés.

Si on cache les commentaires, il ne reste que très peu de code :

```bash
first-test $ grep -v -e "^ *#" Vagrantfile | grep .
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
end
```

lancement :

```bash
first-test $ vagrant up --provider virtualbox
.../...
first-test $ vagrant ssh
[vagrant@localhost ~]$ cat /etc/redhat-release
CentOS Linux release 7.3.1611 (Core)
[vagrant@localhost ~]$ ip a | grep " inet "
    inet 127.0.0.1/8 scope host lo
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic eth0
[vagrant@localhost ~]$ exit
first-test $ vagrant destroy -f
==> default: Forcing shutdown of VM...
==> default: Destroying VM and associated drives...
```

Une VM centOS 7 standard est créée, vous vous y connectez en ssh, puis elle est détruite.

### Configuration de VM

Dans le fichier suivant (Vagrantfile-1) on effectue les confugurations suivantes :

- ajout d'une ip sur un réseaux host-only,
- on supprime la vérification de la version de la box (box_check_update = false),
- définition d'un hostname,
- lancer quelques commandes de **provision**
- et aussi lancer la console graphique VirtualBox(vb.gui = true). (juste pour vous montrer en fait)

```bash
first-test $ cp Vagrantfile-1 Vagrantfile
first-test $ grep -v -e "^ *#" Vagrantfile | grep .
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vm.box_check_update = false
  config.vm.network "private_network", ip: "192.168.56.10"
  config.vm.hostname = "Master"
  config.vm.provision "shell", inline: <<-SHELL
    uptime
    echo "up"
  SHELL
  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
  end
end
```

> A voir : les options de pour la vm : https://www.vagrantup.com/docs/vagrantfile

Lancement :

```bash
first-test $ vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'centos/7'...
==> default: Matching MAC address for NAT networking...
.../...
==> default: Configuring and enabling network interfaces...
==> default: Rsyncing folder: /home/alan/first-test/ => /vagrant
==> default:  17:15:18 up 0 min,  0 users,  load average: 0.38, 0.10, 0.03
==> default: up
first-test $
```

connexion ssh et destruction:

```bash
first-test$ vagrant ssh
[vagrant@Master ~]$ ip a | grep " inet "
    inet 127.0.0.1/8 scope host lo
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic eth0
    inet 192.168.56.10/24 brd 192.168.56.255 scope global eth1
[vagrant@Master ~]$ ls /vagrant
Vagrantfile  Vagrantfile-1  Vagrantfile-2  Vagrantfile-3
[vagrant@Master ~]$ exit
logout
Connection to 127.0.0.1 closed.
first-test$ vagrant destroy -f
==> default: Forcing shutdown of VM...
==> default: Destroying VM and associated drives...
first-test$
```

Vous Noterez :

- La console graphique qui pop
- La sortie des commandes “uptime” et “echo up” penadant la conviguration Vagrant
- Le réseaux host-only créer par vagrant sur VirtualBox (si non existant)
- Sur le guest : la config IP, et le dossier /vagrant ...
- La connexion ssh au travers de votre interface loopback

### Gestion de plusieurs VM

Jusque ici nousn'avon pas vraiment définie de VM, Vagrant en l'absence de cette définition de VM considère une VM par défaut nommée default.

> **Warning.** Pour la suite veillez à bien instancier systématiquement toutes les configs de VM avec un “vm.define” :

Si on utilise le second fichier fournit en exemple :

```bash
first-test $ cp Vagrantfile-2 Vagrantfile
first-test $ cat Vagrantfile  # Après un cp Vagrantfile-2 Vagrantfile
Vagrant.configure(2) do |config|
  config.vm.box = "centos/7"
  config.vm.define "master" do |master|
    master.vm.network "private_network", ip: "192.168.56.10"
    master.vm.hostname = "master"
  end
  (1..2).each do |i|
    config.vm.define "slave-#{i}" do |slave|
      slave.vm.box = "centos/7"
      slave.vm.network "private_network", ip: "192.168.56.3#{i}"
      slave.vm.hostname = "slave-#{i}"
    end
  end
end
first-test $ vagrant up
../..
first-test $ vagrant destroy -f
```

Vous noterez dans le `Vagrantfile` :

- Une valeure est définie par défaut au niveau de la définition de la box (config.vm.box="centos/7"),
- puis la surcharge de celle-ci un peu plus loin au niveau de la machine (slave.vm.box = "centos/7")
- Une boucle sur plusieurs vm en utilisant une iteration sur une liste : (1..2).each do |i|

> N’oubliez pas de détruire les VM de votre projet avant de changer de projet ! : Avec la commande `vagrant destroy -f` vous netoyez les vm associées à votre dossier projet (voir le contenu du dossier ./.vagrant)

### Le provisionning

On distinguera dans les fichiers `Vagrantfile` :

- la definition de la ou des VM : la "box", les accès ssh, le provideurs (ici Virtualbox) ;
- Des provisions : file, shell, et Gestionaires de configurations (ansible, chef, puppet, saltstack etc…)

Rappels des commandes :

- `vagrant up` : création de l’environnement et provisionning initiale ou simple démarrage de l’environnement existant (vagrant halt pour l’ arrêt)
- `vagrant destroy` : destruction de tout
- `vagrant provision` : re-déploiement de l’environnement en re-jouant les provisionneur

#### définitions de provisionning

https://www.vagrantup.com/docs/provisioning/

```bash
first-test $ cp Vagrantfile-3 Vagrantfile
first-test $ cat Vagrantfile
Vagrant.configure(2) do |config|
  config.vm.box = "centos/7"
  config.vm.provision "data", type: "file",
    preserve_order: true,
    source: "../sources",
    destination: "/tmp/"
  config.vm.define "master" do |master|
    master.vm.network "private_network", ip: "192.168.56.10" 
    master.vm.hostname = "master"
    master.vm.provision "data", type: "file",
      source: "../src",
      destination: "/tmp/"
  end
  (1..2).each do |i|
    config.vm.define "slave-#{i}" do |slave|
      slave.vm.network "private_network", ip: "192.168.56.3#{i}"
      slave.vm.hostname = "slave-#{i}"
      slave.vm.provision "slave-only", type: "shell", inline: <<-EOF
        echo "up inside"
      EOF
    end
  end
end
first-test $
```

Vous noterez :

- Le nommage des provisionneurs en plus de la définition du Type “file” (C'est pas dans la doc)
- La surcharge du provisionneur nommé data (défini par défaut puis pour les VM slave-1 et slave2)
- La définition d’un provisionneur slave-only localisé à un “vm.define” (dans la boucle sur les VM)

Lancement :

```bash
first-test $ vagrant up
Bringing machine 'master' up with 'virtualbox' provider...
Bringing machine 'slave-1' up with 'virtualbox' provider...
Bringing machine 'slave-2' up with 'virtualbox' provider…
../..
==> master: Rsyncing folder: /home/alan/first-test/ => /vagrant
==> master: Running provisioner: data (file)…
../..
==> slave-1: Rsyncing folder: /home/alan/first-test/ => /vagrant
==> slave-1: Running provisioner: data (file)...
==> slave-1: Running provisioner: slave-only (shell)…
    slave-1: Running: inline script
==> slave-1: up inside
../..
==> 31
==> slave-2: Running provisioner: data (file)...
==> slave-2: Running provisioner: slave-only (shell)...
    slave-2: Running: inline script
==> slave-2: up inside
first-test $ vagrant ssh master
[vagrant@master ~]$ ls -ald /tmp/src/
drwxrwxr-x. 2 vagrant vagrant 6 Nov 11 18:24 /tmp/src/
[vagrant@master ~]$ ls -ald /tmp/sources/
ls: cannot access /tmp/sources/: No such file or directory
[vagrant@master ~]$ exit
logout
Connection to 127.0.0.1 closed.
first-test $ vagrant ssh slave-1
[vagrant@slave-1 ~]$ ls -ald /tmp/src/
ls: cannot access /tmp/src/: No such file or directory
[vagrant@slave-1 ~]$ ls -ald /tmp/sources/
drwxrwxr-x. 2 vagrant vagrant 6 Nov 11 18:25 /tmp/sources/
```

#### déploiement sélectif

__Avec la surcharge de provisionneur :__

Si on ajoute des fichiers et qu'on relance le provisionning `file` nommé `data`

```bash
first-test $ touch ../src/for-master2
first-test $ touch ../source/for-slave2
first-test $ vagrant provision --provision-with data
==> master: Running provisioner: data (file)...
==> slave-1: Running provisioner: data (file)...
==> slave-2: Running provisioner: data (file)...
first-test $ vagrant ssh master
[vagrant@master ~]$ ls /tmp/src/
for-master
for-master2
[vagrant@master ~]$ logout
Connection to 127.0.0.1 closed.
first-test $ vagrant ssh slave-2
[vagrant@slave-2 ~]$ ls /tmp/sources
for-slave
for-slave2
[vagrant@slave-2 ~]$ logout
Connection to 127.0.0.1 closed.
first-test $
```

1. Les nouveaux fichier sont bien poussés sur les cible.
2. On utilise bien un seul nom de provisionneur `data` qui effectue des actions différentes suivant les hosts sur lesquels ils s'applique.

__Restreindre à un provisionneur, à une VM ou les deux :__

```bash
first-test $ vagrant provision --provision-with slave-only
==> slave-1: Running provisioner: slave-only (shell)...
    slave-1: Running: inline script
==> slave-1:  18:56:57 up 31 min,  0 users,  load average: 0.00, 0.01, 0.03
==> slave-1: up inside
==> slave-2: Running provisioner: slave-only (shell)...
    slave-2: Running: inline script
==> slave-2:  18:56:59 up 30 min,  0 users,  load average: 0.00, 0.01, 0.05
==> slave-2: up inside
first-test $ vagrant provision master
==> master: Running provisioner: data (file)...
first-test $ vagrant provision --provision-with slave-only slave-2
==> slave-2: Running provisioner: slave-only (shell)...
    slave-2: Running: inline script
==> slave-2:  18:57:08 up 31 min,  0 users,  load average: 0.00, 0.01, 0.05
==> slave-2: up inside
first-test $ vagrant provision --provision-with data,slave-only slave-2 master
==> slave-2: Running provisioner: data (file)...
==> slave-2: Running provisioner: slave-only (shell)...
    slave-2: Running: inline script
==> slave-2:  06:27:00 up 12:00,  0 users,  load average: 0.00, 0.01, 0.05
==> slave-2: up inside
==> master: Running provisioner: data (file)...
```

### Exemple d’utilisation sur un projet

Dans le cadre d'un projet on Pourra alors utiliser vagrant afin de founir un environnement de test grandeur nature aux developpeurs :

Vous avez déja une arborescance projet à disposition  :

```bash
first-test $ cd ../../work/projet
projet $ tree  
.
├── src
│   ├── process.yml
│   └── testapp.js
└── vagrant
    ├── task
    │   ├── apply.sh
    │   ├── build.sh
    │   └── init.sh
    └── Vagrantfile
```

- Le script init.sh : installera node.js et pm2
- Le script build.sh : ce script construit l’application ( config / compile)
- Le script apply.sh : prise en compte de la nouvelle version: relance des services.

Dans l’exemple de vagrant file ci-après, vous noterez les options:

- l'utilisation de variables
- run: always : afin de forcer l’exécution à chaque up ou reload des vm. Par défaut c’est seulement au premier up de la VM qu’il est lancé (run: once)
- preserve_order: true : afin de garder l’ordre d’exécution des tache de provision

Vagrantfile  :

```ruby
BASEIP = "192.168.56.3"
BASEHOSTNAME = "web"
Vagrant.configure(2) do |config|
  config.vm.box = "centos/7"
  (1..1).each do |i|
    config.vm.define "#{BASEHOSTNAME}-#{i}" do |web|
      web.vm.hostname = "#{BASEHOSTNAME}-#{i}"
      web.vm.network "private_network", ip: "#{BASEIP}#{i}"
      web.vm.provision "init",
        type: "shell",
        preserve_order: true,
        run: "once",
        inline: <<-EOF
          echo "#### runing initalisation of environement"
          sudo /vagrant/task/init.sh
          echo "#### initialized"
        EOF
      web.vm.provision "sources", type: "file",
        preserve_order: true,
        source: "../src",
        destination: "/opt/"
      web.vm.provision "build", type: "shell",
        preserve_order: true,
        run: "always",
        inline: <<-EOF
          echo "#### runing build "
          sudo /vagrant/task/build.sh
          echo "#### builded"
        EOF
      web.vm.provision "apply", type: "shell",
        preserve_order: true,
        run: "always",
        inline: <<-EOF
          echo "#### applying version "
          sudo /vagrant/task/apply.sh
          echo "#### builded"
        EOF
    end
  end
end
```

Lancement :

```bash
projet $ vagrant up
.../...
```

Commandes de provisioning :

```bash
projet $ vagrant provision --provision-with sources,build,apply
.../...
```

Vous reprovisionner les sources, vous builder l’appli, puis vous redémarer les services

re-installation des dépendances  

```bash
projet $ vagrant provision --provision-with init
.../...
```

## Pour aller plus loin

il est possible de 
- Fabriquer ses propre box https://www.vagrantup.com/docs/virtualbox/boxes.html
- Utiliser des clones lié https://www.vagrantup.com/docs/virtualbox/configuration.html#linked-clones
- Utiliser des provider de VM autres tel Hyperv, KVM, proxmox etc...

Et surtout utiliser une solution de provisionning externe tel [Ansible](./tuto-ansible-fr.md) saltstack chef pupet etc...
