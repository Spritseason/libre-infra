# informatique libre dans les infrastructures

## Présentation

Les outils libres et opensource occupent désormais de fait une place prépondérante dans le monde de l'IT. En effet, avec le temps, le caractère libre des outils et logiciels est devenu un gage de qualité. Cette qualité des outils libres se manifeste à travers plusieurs aspects, parmi lesquels : la sécurité des outils et des processus, la maturité des solutions technologiques et la complète transparence sur le fonctionnement interne.
Cette qualité accrue a permis notamment aux systèmes GNU/Linux de s'imposer de façon drastique chez les techniciens, en particulier dans les infrastructures.

A l'heure où RedHat est l'un des acteurs majeurs de l'IT, où une petite société comme Docker a su se faire une place de choix au sein des écosystèmes Cloud ; mais aussi à l'heure où Microsoft et Google sont des acteurs majeurs du libres et de l'opensource, il est bon de s'intéresser à cette nouvelle façon de créer et innover, de façon plus ouverte.

Cette majeure a donc pour objectif l'appréhension approfondie des infrastructures basées sur des outils libres, ainsi que le développement d'une expertise technique *via* l'étude théorique de certains procédés techniques.  

L'accent sera mis sur le **libre** (et non "l'opensource").

## Objectifs

* Approfondir les connaissances sur le fonctionnement des systèmes GNU/Linux
* Appréhender la place des technologies libres à l'ère du cloud
* Comprendre les bénéfices concrets liés à l'application de la philosophie du libre
* Avoir une vision sur l'écosystème de l'informatique libre
  * cloud
  * déploiement
  * stockage
  * réseau
  * sauvegarde
  * monitoring
  * etc.
* mettre en oeuvre une solution technique basée sur des outils libres
  * couche OS/kernel
  * gestion d'OS et de services
  * maîtrise du packaging (de solutions comme d'infrastructures)
  * virtualisation
  * conteneurisation & clustering
  * déploiement cloud
  * gestion et déploiement de configuration
  * automatisation et infrastructure-as-code
* être en mesure d'assurer la pérennité d'une solution/d'une infrastructure libre

## Notation

### TP

Les TP sont notés en tenant compte des participants de façon individuelle, en se basant sur leurs niveaux et leurs attentes (qui sont différents).
Une grille est définie pour chaque TP ce qui permet d'avoir une vision simple sur qui a fait quoi.

### Questionaires

De façon récurente des petits questionaires permettent d'évaluer le niveau de connaissance.

En fonction des sujets évaluer :

* Un questionaire papier de quelques questions
* l'outil en ligne Kahoot pour un QCM gamifié

### Projet - Oral

L'oral du projet inclut une notation par les pairs.

Pour le projet, des groupes seront formés (3/4 max). Le jour de l'oral, tous les groupes sont présents, et tous les groupes nous font parvenir les notes qu'ils ont attribuées aux autres groupes.

On demande à chaque groupe de noter les autres groupes, selon 4 critères.
Chaque critère représente 5 points.
Un groupe ne note qu'une fois chaque autre groupe.(la notation est de groupe à groupe, pas d'individu à groupe).
Chaque groupe fournit donc une note sur 20 pour tous les autres groupes (4 critères sur 5).
Nous (intervenants) notons aussi chacun des groupes, chaque intervenant compte pour 1 ou 2 groupes.

Les critères :

* pertinence de la problématique    /5
  * est-ce que c'est dans l'ère du temps ? utile ? etc.
* challenge recherché    /5
  * est-ce que l'étudiant s'est challengé en choisissant ce sujet ou est-il rester dans sa zone de confort ?
* qualité de la présentation (visuelle, oral)    /5
  * le moment de la présentation était-il agréable ?
  * la présentation de la problématique et des travaux réalisés était elle concise ? claire ?
* qualité technique de la réponse au besoin    /5
  * est-ce que la solution technique répond correctement au besoin énoncé ?

### Projet - Ecrit

Le rendu du projet sera accompagné d'un dossier, qui pourra aussi faire l'objet d'une note :

* contexte
* documentation de la solution mise en place
* documentation MCO (maintien en conditions opérationnelles), au minimum :
  * redémarrage
  * mise à jour
  * documentation sur les indicateurs de monitoring
    * qu'est-ce qui doit être surveillé et comment ?
  * documentation sauvegarde
    * qu'est-ce qui doit être sauvegardé et comment ?
